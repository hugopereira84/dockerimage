FROM ubuntu:latest
MAINTAINER Hugo Pereira <hugopereira84@gmail.com>

# Keep upstart from complaining
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# Install Packages
RUN apt-get update && apt-get install -my \
  supervisor \
  curl \
  wget \
  php5-curl \
  php5-fpm \
  php5-gd \
  php5-xsl \
  php5-mysqlnd \
  php5-mcrypt \
  php5-xdebug \
  php5-cli \
  php5-intl

# mysql config
ADD configs/mysql/my.cnf /etc/mysql/conf.d/my.cnf
RUN chmod 664 /etc/mysql/conf.d/my.cnf

# php config
RUN sed -i -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 100M/g" /etc/php5/apache2/php.ini
RUN sed -i -e "s/post_max_size\s*=\s*8M/post_max_size = 100M/g" /etc/php5/apache2/php.ini
RUN sed -i -e "s/short_open_tag\s*=\s*Off/short_open_tag = On/g" /etc/php5/apache2/php.ini

# apache config
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
RUN chown -R www-data:www-data /var/www/

# Supervisor Config
RUN mkdir /var/log/supervisor/
RUN /usr/bin/easy_install supervisor
RUN /usr/bin/easy_install supervisor-stdout
ADD configs/supervisord/supervisord.conf /etc/supervisord.conf


# enable mods in machine
RUN a2enmod php5
RUN a2enmod rewrite
RUN /usr/sbin/php5enmod mcrypt



#virtualhost config
#/configs/apache/zapoutbound.local.conf


# Expose mysql
EXPOSE 3306
# Expose apache.
EXPOSE 80

# REMOVE FROM: 
# https://github.com/eugeneware/docker-apache-php 
# https://medium.com/dev-tricks/apache-and-php-on-docker-44faef716150
